import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'basics-assignment-third';
  display = false;
  log = [];

  toggleDisplay(){
    this.display = !this.display;
    if (this.display){
      this.log.push(new Date());
    }
  }
}
